# CTS-R

An R interface for text retrieval from a Canonical Text Service. 

# Example Usage

~~~~
install.packages('devtools')
devtools::install_git("https://git.informatik.uni-leipzig.de/jtiepmar/cts-r/")

print(ctsRetrieval::get_editions("urn:cts:pbc"))
urn <- "urn:cts:pbc:bible.parallel.eng.kingjames:1"
print(ctsRetrieval::get_prev_next(urn))
print(ctsRetrieval::get_valid_reff(urn,3))
print(ctsRetrieval::get_first_urn(urn))
print(ctsRetrieval::get_passage(urn))
print(ctsRetrieval::get_passage_plus(urn))
print(ctsRetrieval::get_label(urn))
~~~~

# Usage Hints

## Internal (Hidden) Requests
Since the server address is requested each time if it is not specified, it is more efficient to request it beforehand using the function get_namespace_url(urn) and then provide it with each subsequent request. Not doing this effectively doubles the amount of subsequent requests.

e.g.:

3 internal requests:
~~~~
urn <- "urn:cts:pbc:bible.parallel.eng.kingjames:1.5"
ns <- ctsRetrieval::get_namespace_url(urn)
print(ctsRetrieval::get_passage(urn, ns))
print(ctsRetrieval::get_label(urn, ns))
~~~~

The same result but 4 internal requests:
~~~~
urn <- "urn:cts:pbc:bible.parallel.eng.kingjames:1.5"
print(ctsRetrieval::get_passage(urn))
print(ctsRetrieval::get_label(urn))
~~~~

# Why?

This package provides R-acchess to text data that is published via a Canonical Text Service by wrapping the HTTP-requests in R-functions. The namespace and server address is set up automatically based on the URN that is provided and the server address that is published via the Namespace Resolver webservice. 
This package provides R-acchess to text data that is published via a Canonical Text Service by wrapping the HTTP-requests in R-functions. 
The server address is set up automatically based on the URN that is provided and the server address that is published via the Namespace Resolver webservice. A server address can be given as an optional parameter if a local CTS instance is used. 

